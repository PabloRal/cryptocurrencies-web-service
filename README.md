Cryptocurrencies

When you run Application will be available on localhost:8080

Example of usage. 

/currencies/{currencyIso} <br>
Use GET method to get current rates for cryptocurrency you are interested in.<br>
Example: <br>
Query: /currencies/BTC<br>
Should return something like that:
```json
{
    "rates": {
        "MATIC": 0.000001761490713687,
        "XEM": 0.000005215184200275,
        "BEST*": 0.000014251590549463,
        "FTT**": 0.000378419995430569,
        "HT": 0.000545450012118458,
        "STEEM": 0.00002150885863408,
        "DGD": 0.004715818598459797,
        "ZRX": 0.000024419717414016,
        "BCD": 0.000073585453329994,
        "KMD": 0.000079900950697857,
        "BSV": 0.0268340745485377,
        "IOST": 4.45986883352E-7,
        "BCH": 0.03198626890303609,
        "CENNZ": 0.000007831704012966,
        ...
    },
    "source": "BTC"
}
```

You can also filter number of result to get only those currencies you want.<br>
Example: <br>
Query: /currencies/BTC?filter=XRP&filter=ETH&filter=BTC<br>
Should return something like that:
```json
{
    "rates": {
        "BTC": 1,
        "XRP": 0.000026624882050664,
        "ETH": 0.024883591002815328
    },
    "source": "BTC"
}
```

/currencies/exchange<br>
Use POST method to get forecast of cryptocurrency exchange.<br>
For request body:
```json
{
	"from":"BTC",
	"to":
	[
		"ETH",
		"XRP"
	],
	"amount":19
}
```
You will get:
```json
{
    "from": "BTC",
    "forecast": {
        "XRP": {
            "rate": 0.000026608120625934,
            "amount": 19,
            "result": 0.00051060983481167346,
            "fee": 0.00000505554291892746
        },
        "ETH": {
            "rate": 0.0249128168326482,
            "amount": 19,
            "result": 0.478076955018518958,
            "fee": 0.004733435198203158
        }
    }
}
```
For each value in field "result" fee is already added.

Data provided by Coinranking