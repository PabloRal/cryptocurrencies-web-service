package com.talar.cryptocurrencies.services.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;
import java.net.ConnectException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.talar.cryptocurrencies.dtos.CoinRankingApiData;
import com.talar.cryptocurrencies.dtos.CoinRankingCoinsApiResponse;


@RunWith(MockitoJUnitRunner.class)
public class CoinRankingClientTest
{
    private static final String XRP_SYMBOL = "XRP";
    private static final String BTC_SYMBOL = "BTC";
    private static final String USDT_SYMBOL = "USDT";
    private static final String BTC_RATE = "1.012000";
    private static final String XRP_RATE = "0.00123";
    private static final String REQUEST_URI = "https://api.coinranking.com/v1/public/coins";
    private static final String REQUEST_PARAMS = "?limit=100&base=USDT";

    @InjectMocks
    private CoinRankingClient client = new CoinRankingClient();

    @Mock
    private RestTemplate restTemplate;

    @Test
    public void shouldReturnProperMapWhenRestTemplateReturnsResponseWithProperData()
    {
        Mockito.when(restTemplate.getForEntity(REQUEST_URI.concat(REQUEST_PARAMS), CoinRankingCoinsApiResponse.class))
                .thenThrow(new RestClientException("Error"));

        assertThrows(ConnectException.class, () -> client.getExchangeRatesFor(USDT_SYMBOL));
    }

    @Test
    public void shouldThrowConnectionExceptionWhenRestTemplateThrowsResTemplateException() throws ConnectException
    {
        //given
        Mockito.when(restTemplate.getForEntity(REQUEST_URI.concat(REQUEST_PARAMS), CoinRankingCoinsApiResponse.class))
                .thenReturn(getResponseEntity());

        //when
        final Map<String, BigDecimal> response = client.getExchangeRatesFor(USDT_SYMBOL);

        //then
        assertEquals(getExpectedResponse(), response);
    }

    private ResponseEntity<CoinRankingCoinsApiResponse> getResponseEntity()
    {
        CoinRankingApiData data = new CoinRankingApiData(Collections.emptyMap(), Collections.emptyMap(),
                getListOfCointForCoinRankingApiResponse());
        CoinRankingCoinsApiResponse body = new CoinRankingCoinsApiResponse("200", data);
        return new ResponseEntity<>(body, HttpStatus.OK);
    }

    private Map<String, BigDecimal> getExpectedResponse()
    {
        HashMap<String, BigDecimal> map = new HashMap<>();
        map.put(XRP_SYMBOL, new BigDecimal(XRP_RATE));
        map.put(BTC_SYMBOL, new BigDecimal(BTC_RATE));
        return map;
    }

    private List<Map<String, Object>> getListOfCointForCoinRankingApiResponse()
    {
        List<Map<String, Object>> list = new ArrayList<>();
        Map<String, Object> xrpData = new HashMap<>();
        Map<String, Object> btcData = new HashMap<>();
        xrpData.put("symbol", XRP_SYMBOL);
        xrpData.put("price", XRP_RATE);
        btcData.put("symbol", BTC_SYMBOL);
        btcData.put("price", BTC_RATE);
        list.add(xrpData);
        list.add(btcData);
        return list;
    }
}
