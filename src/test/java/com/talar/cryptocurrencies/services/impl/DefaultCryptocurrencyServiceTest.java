package com.talar.cryptocurrencies.services.impl;

import java.math.BigDecimal;
import java.net.ConnectException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.assertj.core.api.SoftAssertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.talar.cryptocurrencies.dtos.ExchangeRates;
import com.talar.cryptocurrencies.services.CryptocurrencyRestApiClient;


@RunWith(MockitoJUnitRunner.class)
public class DefaultCryptocurrencyServiceTest
{
    private static final String BTC_ISO = "BTC";
    private static final BigDecimal BTC_RATE = new BigDecimal("1");
    private static final String XRP_ISO = "XRP";
    private static final BigDecimal XRP_RATE = new BigDecimal("0.5");
    private static final String USDT_ISO = "USDT";
    private static final BigDecimal USDT_RATE = new BigDecimal("0.25");

    @InjectMocks
    private DefaultCryptocurrencyService service = new DefaultCryptocurrencyService();

    @Mock
    private CryptocurrencyRestApiClient cryptocurrencyRestApiClient;

    private SoftAssertions softAssertions;

    @Before
    public void setUp() throws Exception
    {
        Mockito.when(cryptocurrencyRestApiClient.getExchangeRatesFor(BTC_ISO)).thenReturn(getRatesMap());
        softAssertions = new SoftAssertions();
    }

    @Test
    public void shouldReturnMapQuotationsWithAllCurrenciesGivenByApiWhenNoFilters() throws ConnectException
    {
        //give
        ExchangeRates expectedRates = new ExchangeRates(BTC_ISO, getRatesMap());

        //when
        final ExchangeRates rates = service.getRates("BTC", Collections.emptyList());

        //then
        softAssertions.assertThat(expectedRates.getSource().equals(rates.getSource())).isEqualTo(true);
        softAssertions.assertThat(areMapsEqual(expectedRates.getRates(), rates.getRates())).isEqualTo(true);
        softAssertions.assertAll();
    }

    @Test
    public void shouldReturnMapQuotationsWithFilteredOutCurrenciesWhenFiltersExist() throws ConnectException
    {
        //give
        ExchangeRates expectedRates = new ExchangeRates(BTC_ISO, getSingletonRateMap());

        //when
        final ExchangeRates rates = service.getRates("BTC", Collections.singletonList("XRP"));

        //then
        softAssertions.assertThat(expectedRates.getSource().equals(rates.getSource())).isEqualTo(true);
        softAssertions.assertThat(areMapsEqual(expectedRates.getRates(), rates.getRates())).isEqualTo(true);

        softAssertions.assertAll();
    }

    private boolean areMapsEqual(final Map<String, BigDecimal> first, final Map<String, BigDecimal> second)
    {
        if (first.size() != second.size())
        {
            return false;
        }
        return first.entrySet().stream()
                .allMatch(entry -> entry.getValue().equals(second.get(entry.getKey())));
    }

    private HashMap<String, BigDecimal> getRatesMap()
    {
        final HashMap<String, BigDecimal> rates = new HashMap<>();
        rates.put(XRP_ISO, XRP_RATE);
        rates.put(USDT_ISO, USDT_RATE);
        rates.put(BTC_ISO, BTC_RATE);
        return rates;
    }

    private Map<String, BigDecimal> getSingletonRateMap()
    {
        return Collections.singletonMap(XRP_ISO, XRP_RATE);
    }
}
