package com.talar.cryptocurrencies.services.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;

import com.talar.cryptocurrencies.dtos.SingleExchangeForecast;


class DefaultCryptocurrencyCalculatorTest
{

    private static final String XRP_ISO = "XRP";
    private static final String USDT_ISO = "USDT";
    private static final String BTC_ISO = "BTC";
    private static final BigDecimal XRP_RATE = new BigDecimal("0.2");
    private static final BigDecimal USDT_RATE = new BigDecimal("0.02");
    private static final BigDecimal BTC_RATE = new BigDecimal("1");

    private DefaultCryptocurrencyCalculator calculator = new DefaultCryptocurrencyCalculator();

    private SoftAssertions softAssertions = new SoftAssertions();

    @Test
    public void shouldReturnResultWhenProperDataGiven()
    {
        //given
        final Map<String, BigDecimal> exchangeRates = getProperExchangeRates();
        final BigDecimal amount = new BigDecimal("1");

        //when
        final SingleExchangeForecast singleExchangeForecast = calculator.calculateExchangeForecast(XRP_ISO, amount,
                exchangeRates);

        //then
        softAssertions.assertThat(singleExchangeForecast.getAmount()).isEqualTo(amount);
        softAssertions.assertThat(singleExchangeForecast.getFee()).isEqualTo(new BigDecimal("0.002"));
        softAssertions.assertThat(singleExchangeForecast.getResult()).isEqualTo(new BigDecimal("0.202"));
        softAssertions.assertThat(singleExchangeForecast.getRate()).isEqualTo(XRP_RATE);
        softAssertions.assertAll();
    }

    private Map<String, BigDecimal> getProperExchangeRates()
    {
        final HashMap<String, BigDecimal> exchangeMap = new HashMap<>();
        exchangeMap.put(XRP_ISO, XRP_RATE);
        exchangeMap.put(USDT_ISO, USDT_RATE);
        exchangeMap.put(BTC_ISO, BTC_RATE);
        return exchangeMap;
    }
}
