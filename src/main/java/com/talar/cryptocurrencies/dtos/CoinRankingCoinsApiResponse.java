package com.talar.cryptocurrencies.dtos;

public class CoinRankingCoinsApiResponse
{
    public String status;
    public CoinRankingApiData data;

    public CoinRankingCoinsApiResponse()
    {

    }

    public CoinRankingCoinsApiResponse(final String status, final CoinRankingApiData data)
    {
        this.status = status;
        this.data = data;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(final String status)
    {
        this.status = status;
    }

    public CoinRankingApiData getData()
    {
        return data;
    }

    public void setData(final CoinRankingApiData data)
    {
        this.data = data;
    }
}
