package com.talar.cryptocurrencies.dtos;

import java.util.List;
import java.util.Map;


public class CoinRankingApiData
{
    private Map<String, Object> stats;
    private Map<String, String> base;
    private List<Map<String, Object>> coins;

    public CoinRankingApiData()
    {

    }

    public CoinRankingApiData(final Map<String, Object> stats, final Map<String, String> base,
            final List<Map<String, Object>> coins)
    {
        this.stats = stats;
        this.base = base;
        this.coins = coins;
    }

    public Map<String, Object> getStats()
    {
        return stats;
    }

    public void setStats(final Map<String, Object> stats)
    {
        this.stats = stats;
    }

    public Map<String, String> getBase()
    {
        return base;
    }

    public void setBase(final Map<String, String> base)
    {
        this.base = base;
    }

    public List<Map<String, Object>> getCoins()
    {
        return coins;
    }

    public void setCoins(final List<Map<String, Object>> coins)
    {
        this.coins = coins;
    }
}
