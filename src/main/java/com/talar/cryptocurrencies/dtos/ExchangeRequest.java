package com.talar.cryptocurrencies.dtos;

import java.math.BigDecimal;
import java.util.List;


public class ExchangeRequest
{
    private String from;
    private List<String> to;
    private BigDecimal amount;

    public ExchangeRequest(final String from, final List<String> to, final BigDecimal amount)
    {
        this.from = from;
        this.to = to;
        this.amount = amount;
    }

    public String getFrom()
    {
        return from;
    }

    public void setFrom(final String from)
    {
        this.from = from;
    }

    public List<String> getTo()
    {
        return to;
    }

    public void setTo(final List<String> to)
    {
        this.to = to;
    }

    public BigDecimal getAmount()
    {
        return amount;
    }

    public void setAmount(final BigDecimal amount)
    {
        this.amount = amount;
    }
}
