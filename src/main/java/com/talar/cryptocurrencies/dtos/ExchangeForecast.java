package com.talar.cryptocurrencies.dtos;

import java.util.Map;


public class ExchangeForecast
{
    private String from;
    private Map<String, SingleExchangeForecast> forecast;

    public ExchangeForecast(final String from, final Map<String, SingleExchangeForecast> forecast)
    {
        this.from = from;
        this.forecast = forecast;
    }

    public String getFrom()
    {
        return from;
    }

    public void setFrom(final String from)
    {
        this.from = from;
    }

    public Map<String, SingleExchangeForecast> getForecast()
    {
        return forecast;
    }

    public void setForecast(final Map<String, SingleExchangeForecast> forecast)
    {
        this.forecast = forecast;
    }
}
