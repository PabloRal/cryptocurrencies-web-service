package com.talar.cryptocurrencies.dtos;

import java.math.BigDecimal;
import java.util.Map;


public class ExchangeRates
{
    private Map<String, BigDecimal> rates;
    private String source;

    public ExchangeRates(final String currencyIso, final Map<String, BigDecimal> exchangeRates)
    {
        this.source = currencyIso;
        this.rates = exchangeRates;
    }

    public Map<String, BigDecimal> getRates()
    {
        return rates;
    }

    public void setRates(final Map<String, BigDecimal> rates)
    {
        this.rates = rates;
    }

    public String getSource()
    {
        return source;
    }

    public void setSource(final String source)
    {
        this.source = source;
    }
}

