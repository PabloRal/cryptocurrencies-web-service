package com.talar.cryptocurrencies.dtos;

import java.math.BigDecimal;


public class SingleExchangeForecast
{
    private BigDecimal rate;
    private BigDecimal amount;
    private BigDecimal result;
    private BigDecimal fee;

    public SingleExchangeForecast(final BigDecimal rate, final BigDecimal amount, final BigDecimal result, final BigDecimal fee)
    {
        this.rate = rate;
        this.amount = amount;
        this.result = result;
        this.fee = fee;
    }

    public BigDecimal getRate()
    {
        return rate;
    }

    public void setRate(final BigDecimal rate)
    {
        this.rate = rate;
    }

    public BigDecimal getAmount()
    {
        return amount;
    }

    public void setAmount(final BigDecimal amount)
    {
        this.amount = amount;
    }

    public BigDecimal getResult()
    {
        return result;
    }

    public void setResult(final BigDecimal result)
    {
        this.result = result;
    }

    public BigDecimal getFee()
    {
        return fee;
    }

    public void setFee(final BigDecimal fee)
    {
        this.fee = fee;
    }
}
