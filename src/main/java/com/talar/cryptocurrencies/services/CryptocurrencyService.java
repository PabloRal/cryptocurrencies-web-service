package com.talar.cryptocurrencies.services;

import java.math.BigDecimal;
import java.net.ConnectException;
import java.util.List;

import com.talar.cryptocurrencies.dtos.ExchangeForecast;
import com.talar.cryptocurrencies.dtos.ExchangeRates;


public interface CryptocurrencyService
{
    ExchangeRates getRates(String currencyIso, final List<String> filterRates) throws ConnectException;

    ExchangeForecast getExchangeForecast(String baseIso, BigDecimal amount, List<String> currencies) throws ConnectException;
}
