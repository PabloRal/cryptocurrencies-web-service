package com.talar.cryptocurrencies.services.impl;

import static java.util.Objects.requireNonNull;

import java.math.BigDecimal;
import java.net.ConnectException;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.talar.cryptocurrencies.dtos.CoinRankingCoinsApiResponse;
import com.talar.cryptocurrencies.services.CryptocurrencyRestApiClient;


@Component
public class CoinRankingClient implements CryptocurrencyRestApiClient
{
    private static final int COINS_LIMIT = 100;
    private static final String URI = "https://api.coinranking.com/v1/public/coins";
    private static final String BASE_CURRENCY_PARAM = "base";
    private static final String FILTER_PARAM = "limit";

    private RestTemplate restTemplate = new RestTemplate();

    @Override
    public Map<String, BigDecimal> getExchangeRatesFor(final String currencyIso) throws ConnectException
    {
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(URI).queryParam(FILTER_PARAM, COINS_LIMIT)
                .queryParam(BASE_CURRENCY_PARAM, currencyIso);
        ResponseEntity<CoinRankingCoinsApiResponse> response;
        try
        {
            response = restTemplate.getForEntity(uriBuilder.toUriString(), CoinRankingCoinsApiResponse.class);
        }
        catch (RestClientException e)
        {
            throw new ConnectException("Cannot connect with CoinRanking API");
        }
        return requireNonNull(response.getBody()).getData().getCoins().stream().collect(
                Collectors.toMap(coin -> coin.get("symbol").toString(), coin -> new BigDecimal(coin.get("price").toString())));
    }

    public void setRestTemplate(final RestTemplate restTemplate)
    {
        this.restTemplate = restTemplate;
    }
}

