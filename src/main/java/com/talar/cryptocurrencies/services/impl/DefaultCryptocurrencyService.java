package com.talar.cryptocurrencies.services.impl;

import java.math.BigDecimal;
import java.net.ConnectException;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.talar.cryptocurrencies.dtos.ExchangeForecast;
import com.talar.cryptocurrencies.dtos.ExchangeRates;
import com.talar.cryptocurrencies.dtos.SingleExchangeForecast;
import com.talar.cryptocurrencies.services.CryptocurrencyCalculator;
import com.talar.cryptocurrencies.services.CryptocurrencyRestApiClient;
import com.talar.cryptocurrencies.services.CryptocurrencyService;


@Service
public class DefaultCryptocurrencyService implements CryptocurrencyService
{
    @Resource(name = "coinRankingClient")
    private CryptocurrencyRestApiClient cryptocurrencyApi;
    @Resource(name = "defaultCryptocurrencyCalculator")
    private CryptocurrencyCalculator cryptocurrencyCalculator;

    @Override
    public ExchangeRates getRates(final String currencyIso, final List<String> filterRates) throws ConnectException
    {

        final Map<String, BigDecimal> exchangeRates = filterOutUnnecessaryRates(
                cryptocurrencyApi.getExchangeRatesFor(currencyIso), filterRates);
        return new ExchangeRates(currencyIso, exchangeRates);
    }

    @Override
    public ExchangeForecast getExchangeForecast(final String baseIso, final BigDecimal amount, final List<String> currencies)
            throws ConnectException
    {
        final Map<String, SingleExchangeForecast> forecastList = createForecastMap(currencies, amount, baseIso);
        return new ExchangeForecast(baseIso, forecastList);
    }

    private Map<String, SingleExchangeForecast> createForecastMap(final List<String> currencies, final BigDecimal amount,
            final String baseIso) throws ConnectException
    {
        final Map<String, BigDecimal> exchangeRates = cryptocurrencyApi.getExchangeRatesFor(baseIso);
        return currencies.stream().collect(Collectors.toMap(Function.identity(),
                quoteIso -> cryptocurrencyCalculator.calculateExchangeForecast(quoteIso, amount, exchangeRates)));
    }

    private Map<String, BigDecimal> filterOutUnnecessaryRates(final Map<String, BigDecimal> exchangeRates,
            final List<String> filterRates)
    {
        return filterRates.isEmpty() ? exchangeRates
                : exchangeRates.entrySet().stream().filter(entry -> filterRates.contains(entry.getKey()))
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }
}
