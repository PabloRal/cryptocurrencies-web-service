package com.talar.cryptocurrencies.services.impl;

import java.math.BigDecimal;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.talar.cryptocurrencies.dtos.SingleExchangeForecast;
import com.talar.cryptocurrencies.services.CryptocurrencyCalculator;

@Service
public class DefaultCryptocurrencyCalculator implements CryptocurrencyCalculator
{
    @Override
    public SingleExchangeForecast calculateExchangeForecast(final String quoteIso, final BigDecimal amount,
            final Map<String, BigDecimal> exchangeRates)
    {
        final BigDecimal rate = exchangeRates.get(quoteIso);
        final BigDecimal result = exchangeRates.get(quoteIso).multiply(amount);
        final BigDecimal fee = calculateFee(result);
        final BigDecimal resultWithFee = result.add(fee);
        return new SingleExchangeForecast(rate, amount, resultWithFee, fee);
    }

    private BigDecimal calculateFee(final BigDecimal result)
    {
        return BigDecimal.valueOf(0.01).multiply(result);
    }
}
