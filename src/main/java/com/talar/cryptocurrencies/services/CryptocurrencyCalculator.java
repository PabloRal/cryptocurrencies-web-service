package com.talar.cryptocurrencies.services;

import java.math.BigDecimal;
import java.util.Map;

import com.talar.cryptocurrencies.dtos.SingleExchangeForecast;


public interface CryptocurrencyCalculator
{
    SingleExchangeForecast calculateExchangeForecast(final String quoteIso, final BigDecimal amount,
            final Map<String, BigDecimal> exchangeRates);
}
