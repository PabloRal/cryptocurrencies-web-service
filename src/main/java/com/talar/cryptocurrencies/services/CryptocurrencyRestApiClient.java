package com.talar.cryptocurrencies.services;

import java.math.BigDecimal;
import java.net.ConnectException;
import java.util.Map;


public interface CryptocurrencyRestApiClient
{
    Map<String, BigDecimal> getExchangeRatesFor(String currencyIso) throws ConnectException;
}
