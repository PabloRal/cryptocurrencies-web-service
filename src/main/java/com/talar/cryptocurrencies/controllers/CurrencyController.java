package com.talar.cryptocurrencies.controllers;

import java.net.ConnectException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.talar.cryptocurrencies.dtos.ExchangeForecast;
import com.talar.cryptocurrencies.dtos.ExchangeRates;
import com.talar.cryptocurrencies.dtos.ExchangeRequest;
import com.talar.cryptocurrencies.services.CryptocurrencyService;


@RestController
@RequestMapping("/currencies")
public class CurrencyController
{

    private final CryptocurrencyService cryptocurrencyService;

    public CurrencyController(final CryptocurrencyService cryptocurrencyService)
    {
        this.cryptocurrencyService = cryptocurrencyService;
    }

    @GetMapping("/{currencyIso}")
    public ResponseEntity<ExchangeRates> showExchangeRates(@PathVariable final String currencyIso,
            @RequestParam Optional<List<String>> filter)
    {
        final List<String> filterRates = filter.orElse(Collections.emptyList());
        final ExchangeRates exchangeRates;
        try
        {
            exchangeRates = cryptocurrencyService.getRates(currencyIso, filterRates);
            return new ResponseEntity<>(exchangeRates, HttpStatus.OK);
        }
        catch (ConnectException e)
        {
            return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    @PostMapping("/exchange")
    public ResponseEntity<ExchangeForecast> showExchangeForecast(@RequestBody ExchangeRequest request)
    {
        final ExchangeForecast exchangeForecast;
        try
        {
            exchangeForecast = cryptocurrencyService.getExchangeForecast(request.getFrom(), request.getAmount(), request.getTo());
            return new ResponseEntity<>(exchangeForecast, HttpStatus.CREATED);
        }
        catch (ConnectException e)
        {
            return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
        }
    }
}
